Filename: Report_Roderick.md
Purpose: For reporting on the findings of Assignment 1 of the ISA IM Program by Upskill on Big O Notation.
Written by: Roderick Momin
Date Created: 3rd June 2021
Date Modified: 3rd June 2021

Based upon the findings in the assignment it can be stated (at least in hindsight) that the best equation from best to worst are Logarithmic, Linear, Quadratic, Cubic, Polynomial.

Of course in reality the answer is as always "it depends". There are limitations on the plotting of the graph in this assignment due to the input values and thus may not give a full representation of the functions/equations being used. 

Moreover, the time to code, human factors and other things must be weighed against the time to execute. For example, my code's execution time can be improved by hardcoding the values to plot out the graphs and achieve the same goal of showing the graph as opposed to making functions to automate the calculation process to plot out the graph. Execution time will be less if hardcoded but it might not be desirable due to possible human error and tedious code changing should it be required on future use cases.

Other factors to consider would be Tpyical Case Scenario vs. Worst Case Scenario vs. Best Case Scenario.

Each equation especially at smaller numbers are fit for their own use case scenario and might be faster at certain N values than other equations thus upsetting the order of the best equations stated in the beginning of this document. For more information please look at this video by Ned Batchelder. https://youtu.be/Ee0HzlnIYWQ?t=1383

At the end it depends on the usage scenario and goal of the program and codes should be used accordingly (if possible) to achieve the best execution time (after weighing it against other things). For Big Data, code execution time should follow Logarithmic or O(1) equations vs the size of the Data but for smaller data other forms of the equations might be better when comparing time to the data size.

